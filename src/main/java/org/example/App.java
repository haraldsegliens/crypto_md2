package org.example;

import java.nio.file.Path;
import java.util.Date;

public class App {
    public static void main( String[] args ) {
        if(args.length == 6 && args[0].equals("generate")) {
            String issuerDN = args[1];
            long notBeforeDifferenceInDaysBetweenCurrentTime = Long.parseLong(args[2]);
            long notAfterDifferenceInDaysBetweenCurrentTime = Long.parseLong(args[3]);
            String subjectDN = args[4];
            String savingPrefix = args[5];

            CryptoWithSelfSignedCertificate cryptoWithSelfSignedCertificate = new CryptoWithSelfSignedCertificate();
            cryptoWithSelfSignedCertificate.createAndSaveCertificateAndPrivateKey(
                    issuerDN,
                    new Date(System.currentTimeMillis() + notBeforeDifferenceInDaysBetweenCurrentTime * 60 * 60 * 24),
                    new Date(System.currentTimeMillis() + notAfterDifferenceInDaysBetweenCurrentTime * 60 * 60 * 24),
                    subjectDN,
                    Path.of(System.getProperty("user.dir")),
                    savingPrefix
            );
        } else if(args.length == 2 && args[0].equals("verify")) {
            String prefix = args[1];

            CryptoWithSelfSignedCertificate cryptoWithSelfSignedCertificate = new CryptoWithSelfSignedCertificate();
            try {
                cryptoWithSelfSignedCertificate.performCertificateValidation(
                        Path.of(System.getProperty("user.dir")),
                        prefix
                );
            } catch(Throwable ex) {
                System.out.println("Certificate validation has failed!");
                throw ex;
            }
            System.out.println("Certificate validation has succeeded!");
        } else if(args.length == 4 && args[0].equals("encrypt")) {
            String prefix = args[1];
            String plaintextFileName = args[2];
            String encryptedFileName = args[3];

            CryptoWithSelfSignedCertificate cryptoWithSelfSignedCertificate = new CryptoWithSelfSignedCertificate();
            cryptoWithSelfSignedCertificate.encryptMessage(
                    Path.of(System.getProperty("user.dir")),
                    prefix,
                    Path.of(plaintextFileName),
                    Path.of(encryptedFileName)
            );
        } else if(args.length == 4 && args[0].equals("decrypt")) {
            String prefix = args[1];
            String encryptedFileName = args[2];
            String decryptedPlaintextFileName = args[3];

            CryptoWithSelfSignedCertificate cryptoWithSelfSignedCertificate = new CryptoWithSelfSignedCertificate();
            cryptoWithSelfSignedCertificate.decryptMessage(
                    Path.of(System.getProperty("user.dir")),
                    prefix,
                    Path.of(encryptedFileName),
                    Path.of(decryptedPlaintextFileName)
            );
        } else {
            System.out.println(
                    "generate issuerDN notBeforeDifferenceInDaysBetweenCurrentTime notAfterDifferenceInDaysBetweenCurrentTime subjectDN savingPrefix\n" +
                            "\tissuerDN - certificate issuer in RFC4519 format. Example: CN=cn, O=o, L=L, ST=il, C= c\n" +
                            "\tnotBeforeDifferenceInDaysBetweenCurrentTime - days after current time when certificate will be active\n" +
                            "\tnotAfterDifferenceInDaysBetweenCurrentTime - days after current time when certificate will be expired\n" +
                            "\tsubjectDN - certificate subject in RFC4519 format. Example: CN=cn, O=o, L=L, ST=il, C= c\n" +
                            "\tsavingPrefix - string that will be added in front of certificate and private key file name in working directory");
            System.out.println(
                    "verify prefix\n" +
                            "\tprefix - string that was added in front of certificate and private key file name in working directory");
            System.out.println(
                    "encrypt prefix plaintextFileName encryptedFileName\n" +
                            "\tprefix - string that was added in front of certificate and private key file name in working directory\n" +
                            "\tplaintextFileName - file name of the file that is located in working directory that must be encrypted\n" +
                            "\tencryptedFileName - file name of the file that is located in working directory where encrypted result must be saved!");
            System.out.println(
                    "decrypt prefix encryptedFileName decryptedPlaintextFileName\n" +
                            "\tprefix - string that was added in front of certificate and private key file name in working directory\n" +
                            "\tplaintextFileName - file name of the file that is located in working directory that must be decrypted\n" +
                            "\tdecryptedPlaintextFileName - file name of the file that is located in working directory where decrpyted result must be saved");
        }
    }
}

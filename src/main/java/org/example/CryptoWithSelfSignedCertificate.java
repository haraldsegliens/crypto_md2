package org.example;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.CertException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMException;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

public class CryptoWithSelfSignedCertificate {
    private static final String CERTIFICATE_ALGORITHM = "RSA";
    private static final int CERTIFICATE_BITS = 2048;

    static {
        // adds the Bouncy castle provider to java security
        Security.addProvider(new BouncyCastleProvider());
    }

    public void createAndSaveCertificateAndPrivateKey(String issuerDN, Date notBefore, Date notAfter, String subjectDN, Path savingDirectory, String savingPrefix) {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance(CERTIFICATE_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Failed to getInstance for keypair generator", e);
        }
        keyPairGenerator.initialize(CERTIFICATE_BITS, new SecureRandom());
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        // GENERATE THE X509 CERTIFICATE
        X509v3CertificateBuilder v3CertBuilder =  new X509v3CertificateBuilder(
                new X500Name(RFC4519Style.INSTANCE,RFC4519Style.INSTANCE.fromString(issuerDN)),
                BigInteger.valueOf(System.currentTimeMillis()),
                notBefore, notAfter,
                new X500Name(RFC4519Style.INSTANCE,RFC4519Style.INSTANCE.fromString(subjectDN)),
                SubjectPublicKeyInfo.getInstance(keyPair.getPublic().getEncoded())
        );
        ContentSigner signer = null;
        try {
            signer = new JcaContentSignerBuilder("SHA256WithRSAEncryption")
                    .build(keyPair.getPrivate());
        } catch (OperatorCreationException e) {
            throw new RuntimeException("Failed to get content signer", e);
        }
        X509CertificateHolder certHolder = v3CertBuilder.build(signer);

        //saving files
        saveCertificate(certHolder, savingDirectory, savingPrefix);
        saveKeyPair(keyPair, savingDirectory, savingPrefix);
    }

    public void performCertificateValidation(Path directory, String prefix) {
        KeyPair keyPair = loadKeyPair(directory, prefix);
        X509CertificateHolder certificateHolder = loadCertificate(directory, prefix);

        X509Certificate cert = null;
        try {
            cert = new JcaX509CertificateConverter()
                    .setProvider(BouncyCastleProvider.PROVIDER_NAME)
                    .getCertificate(certificateHolder);
        } catch (CertificateException e) {
            throw new RuntimeException("Failed to parse certificate!", e);
        }

        try {
            cert.verify(keyPair.getPublic());
        } catch (CertificateException | NoSuchAlgorithmException | InvalidKeyException | NoSuchProviderException | SignatureException e) {
            throw new RuntimeException("First certificate validation has failed!", e);
        }

        ContentVerifierProvider contentVerifierProvider = null;
        try {
            contentVerifierProvider = new JcaContentVerifierProviderBuilder()
                    .setProvider(BouncyCastleProvider.PROVIDER_NAME)
                    .build(keyPair.getPublic());
        } catch (OperatorCreationException e) {
            throw new RuntimeException("Failed to generate signature validator", e);
        }
        try {
            if (!certificateHolder.isSignatureValid(contentVerifierProvider)) {
                throw new RuntimeException("second (signature) validation has failed");
            }
        } catch (CertException e) {
            throw new RuntimeException("Failed to perform second (signature) validation!", e);
        }
    }

    public void encryptMessage(Path directory, String prefix, Path plainTextFile, Path encryptedFile) {
        X509CertificateHolder certificateHolder = loadCertificate(directory, prefix);

        Cipher cipher;
        try {
            cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get cipher algorithm!", e);
        }

        PublicKey certificatePublicKey;
        try {
            certificatePublicKey = new JcaPEMKeyConverter().getPublicKey(certificateHolder.getSubjectPublicKeyInfo());
        } catch (PEMException e) {
            throw new RuntimeException("Failed to get public key from certificate!", e);
        }

        try {
            cipher.init(Cipher.ENCRYPT_MODE, certificatePublicKey);
        } catch (InvalidKeyException e) {
            throw new RuntimeException("Failed to initialize cipher algorithm", e);
        }

        byte[] plainTextBytes;
        try {
            plainTextBytes = Files.readAllBytes(plainTextFile);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read plainText file!", e);
        }

        byte[] encryptedBytes;
        try {
            encryptedBytes = cipher.doFinal(plainTextBytes);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException("Failed to encrypt plainText!", e);
        }

        try {
            Files.write(encryptedFile, encryptedBytes);
        } catch (IOException e) {
            throw new RuntimeException("Failed to save encrypted file!", e);
        }
    }

    public void decryptMessage(Path directory, String prefix, Path encryptedFile, Path decryptedPlainTextFile) {
        KeyPair keyPair = loadKeyPair(directory, prefix);

        Cipher cipher;
        try {
            cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get cipher algorithm!", e);
        }

        try {
            cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
        } catch (InvalidKeyException e) {
            throw new RuntimeException("Failed to initialize cipher algorithm", e);
        }

        byte[] encryptedBytes;
        try {
            encryptedBytes = Files.readAllBytes(encryptedFile);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read encrypted file!", e);
        }

        byte[] decryptedPlainTextBytes;
        try {
            decryptedPlainTextBytes = cipher.doFinal(encryptedBytes);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException("Failed to decrypt the message", e);
        }

        try {
            Files.write(decryptedPlainTextFile, decryptedPlainTextBytes);
        } catch (IOException e) {
            throw new RuntimeException("Failed to save decrypted plaintext file!", e);
        }
    }

    private void saveCertificate(X509CertificateHolder certHolder, Path directory, String prefix) {
        String fileName = Paths.get(directory.toString(), prefix + "certificate.pem").toString();
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            try (JcaPEMWriter pemWriter = new JcaPEMWriter(fileWriter)) {
                pemWriter.writeObject(certHolder);
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to export certificate to PEM format!", e);
        }
    }

    private X509CertificateHolder loadCertificate(Path directory, String prefix) {
        String fileName = Paths.get(directory.toString(), prefix + "certificate.pem").toString();
        try {
            FileReader fileReader = new FileReader(fileName);
            PEMParser pemReader = new PEMParser(fileReader);
            return (X509CertificateHolder) pemReader.readObject();
        } catch (IOException e) {
            throw new RuntimeException("Failed to load certificate that is in PEM format!", e);
        }
    }

    private void saveKeyPair(KeyPair keyPair, Path directory, String prefix) {
        String fileName = Paths.get(directory.toString(), prefix + "key_pair.pem").toString();
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            try (JcaPEMWriter pemWriter = new JcaPEMWriter(fileWriter)) {
                pemWriter.writeObject(keyPair);
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to export key to PEM format!", e);
        }
    }

    private KeyPair loadKeyPair(Path directory, String prefix) {
        String fileName = Paths.get(directory.toString(), prefix + "key_pair.pem").toString();
        try {
            FileReader fileReader = new FileReader(fileName);
            PEMParser pemReader = new PEMParser(fileReader);
            PEMKeyPair pemKeyPair = (PEMKeyPair) pemReader.readObject();
            return new JcaPEMKeyConverter().getKeyPair(pemKeyPair);
        } catch (IOException e) {
            throw new RuntimeException("Failed to load private key that is in PEM format!", e);
        }
    }
}

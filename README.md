# crypto_md2

Homework 2 for course "DatZ6015 : Applied cryptography"

## Notes

JAVA 11

## Commands

This homework has a command line interface that can be used by running the JAR.
This homework saves certificate file and private keys in PEM format and in working directory.

1. generate issuerDN notBeforeDifferenceInDaysBetweenCurrentTime notAfterDifferenceInDaysBetweenCurrentTime subjectDN savingPrefix
	- issuerDN - certificate issuer in RFC4519 format. Example: CN=cn, O=o, L=L, ST=il, C= c
	- notBeforeDifferenceInDaysBetweenCurrentTime - days after current time when certificate will be active
	- notAfterDifferenceInDaysBetweenCurrentTime - days after current time when certificate will be expired
	- subjectDN - certificate subject in RFC4519 format. Example: CN=cn, O=o, L=L, ST=il, C= c
	- savingPrefix - string that will be added in front of certificate and private key file name in working directory
2. verify prefix
	- prefix - string that was added in front of certificate and private key file name in working directory
3. encrypt prefix plaintextFileName encryptedFileName
	- prefix - string that was added in front of certificate and private key file name in working directory
	- plaintextFileName - file name of the file that is located in working directory that must be encrypted
	- encryptedFileName - file name of the file that is located in working directory where encrypted result must be saved!
3. decrypt prefix encryptedFileName decryptedPlaintextFileName
	- prefix - string that was added in front of certificate and private key file name in working directory
	- plaintextFileName - file name of the file that is located in working directory that must be decrypted
	- decryptedPlaintextFileName - file name of the file that is located in working directory where decrpyted result must be saved

## Examples of execution

java -jar ./target/crypto_md2-1.0.1-jar-with-dependencies.jar generate "CN=cn, O=o, L=L, ST=il, C= c" -1000 1000 "CN=cn, O=o, L=L, ST=il, C= c" test_

generates test_key_pair.pem and test_certificate.pem

java -jar ./target/crypto_md2-1.0.1-jar-with-dependencies.jar encrypt test_ plaintext.txt encrypted.txt

java -jar ./target/crypto_md2-1.0.1-jar-with-dependencies.jar decrypt test_ encrypted.txt decrypted_plaintext.txt 

java -jar ./target/crypto_md2-1.0.1-jar-with-dependencies.jar verify test_ 
 